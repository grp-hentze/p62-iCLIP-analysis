# Thomas Schwarzl <schwarzl@embl.de>
import os

"""
htseq-CLIP workflow

Processing of eCLIP data in standard ENCODE format. 

The input are sorted duplicated removed bam files and an annotation file in
gff3 / gtf (gencode) format
"""

# ----------------------------------------------------------------------------------------
# CONFIG FILE
# ----------------------------------------------------------------------------------------
# This is the path to the config file. If you do not want to use the default config 
# specified here, the config file can be manually set when executing snakemake using 
# the --configfile parameter. 
#
#  --configfile FILE     Specify or overwrite the config file of the workflow
#                       (see the docs). Values specified in JSON or YAML
#                        format are available in the global config dictionary
#                        inside the workflow.
#                        
# Location of the default config file

configfile: "config.json"


# ----------------------------------------------------------------------------------------
# TOOL PATHS
# ----------------------------------------------------------------------------------------


OUT_DIR      = config["output_dir"]
GTF          = config["gtf_file"]
GTF_NAME     = config["gtf_short_name"]

CLIP         = config["clip"]
LOG_DIR      = config["log_dir"]
SITES        = config["sites_to_extract"].split()

# annotation parameters
ANNOTATION_PARAMS = config["annotation_params"]

# ----------------------------------------------------------------------------------------
# DEFINE FILE PATHS
# ----------------------------------------------------------------------------------------


ANNOTATION_FILE = expand("{outdir}/gtf/{gtfn}.bed.gz", gtfn=GTF_NAME, outdir=OUT_DIR)
SAMPLES_WILDCARDS = "extract/{sample}.sorted.bed.gz"
                            

# ----------------------------------------------------------------------------------------
# SAMPLES
# ----------------------------------------------------------------------------------------
# Automatically read in all samples

SAMPLES, = glob_wildcards("extract/{samples}_S1.bed.gz")

# ----------------------------------------------------------------------------------------	
# :::::::: ALL :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# ----------------------------------------------------------------------------------------	

print("PROCESSING samples %s" % (', '.join(SAMPLES)))
print("PROCESSING sites %s" % (', '.join(SITES)))
print("junction count_plots read_plots junction_plot extract_SS extract_E1 dexseq")


rule all:
	run:
		print(ANNOTATION_FILE)
		
# ----------------------------------------------------------------------------------------	
# GTF
# ----------------------------------------------------------------------------------------	

		
rule annotation:
	input:
		GTF
	output:
		ANNOTATION_FILE
	log:
		expand("{logs}/annotation_{gtfn}_preprocessing.log", gtfn=GTF_NAME, outdir=OUT_DIR, logs = LOG_DIR)
	params:
		ANNOTATION_PARAMS 
	message:
		"Preprocessing and sorting annotation file {input}"
	shell:
		"{CLIP} annotation -g {input} {params} | sort -k1,1 -k2,2n | gzip 2> {log}"
		
# ----------------------------------------------------------------------------------------	
# SORT EXTRACTED SITES
# ----------------------------------------------------------------------------------------

rule sort:
	input:
		expand("{outdir}/extract/{samples}_{sites}.bed.gz",
		         samples=SAMPLES, outdir=OUT_DIR, sites=SITES)

rule do_sort:		
	input:
		"{OUT_DIR}/extract/{sample}_{site}.temporary.bed.gz"
	output:
		"{OUT_DIR}/extract/{sample}_{site,\w+}.bed.gz"
	log:
		expand("{logs}/{{sample}}_{{site}}.extract.sort.log", logs = LOG_DIR)
	message:
		"sorting {input}"
	shell:
		"zcat {input} | sort -k1,1 -k2,2n | gzip > {output} 2> {log}"
		
# ----------------------------------------------------------------------------------------
# JUNCTION
# ----------------------------------------------------------------------------------------

rule junction:
	input:
		expand("{outdir}/junction/{samples}_{sites}to{gtfn}.txt.gz", samples=SAMPLES, outdir=OUT_DIR, gtfn=GTF_NAME, sites=SITES)
		
rule do_junction:
	input:
		bed = "{OUT_DIR}/extract/{sample}_{site}.bed.gz",
		gtf = ANNOTATION_FILE
	output:
		"{OUT_DIR}/junction/{sample}_{site}to{gtfn}.txt.gz"
	log:
		expand("{logs}/{{sample}}_{{site}}.junction.log", logs = LOG_DIR)
	shell:
		"{CLIP} junction -i {input.bed} -f {input.gtf} -o {output} 2> {log}"
		
		
# ----------------------------------------------------------------------------------------
# COUNT
# ----------------------------------------------------------------------------------------

rule count:
	input:
		expand("{outdir}/counts/{samples}_{sites}to{gtfn}.count.txt.gz", samples=SAMPLES, outdir=OUT_DIR, gtfn=GTF_NAME, sites=SITES)
		
rule do_count:
	input:
		bed = expand("{outdir}/extract/{{sample}}_{{site}}.bed.gz", outdir = OUT_DIR), 
		gtf = ANNOTATION_FILE
	output:
		expand("{outdir}/counts/{{sample}}_{{site}}to{{gtfn}}.count.txt.gz", outdir = OUT_DIR)
	log: 
		expand("{logs}/{{sample}}_{{site}}.count.log", logs = LOG_DIR)
	message:
		"counting {input.bed}"
	shell:
		"{CLIP} count -i {input.bed} -f {input.gtf} -o {output} -c o  2> {log}"
		
# ----------------------------------------------------------------------------------------	
# READ PLOTS
# ----------------------------------------------------------------------------------------	

rule read_plots:
	input:
		expand("{outdir}/plots/reads/{samples}_{sites}/{samples}_{sites}to{gtfn}_reads.html", samples=SAMPLES, gtfn=GTF_NAME, sites=SITES, outdir=OUT_DIR)
		
rule do_read_plot:
	input:
		"{OUT_DIR}/extract/{sample}_{site}.bed.gz"
	output:
		"{OUT_DIR}/plots/reads/{sample}_{site}/{sample}_{site}to{gtfn}_reads.html"
	log:
		expand("{logs}/{{sample}}_{{site}}.rplot.log", logs = LOG_DIR)
	message:
		"creating read plot for {input}"
	shell:
		"PYTHONPATH="" "
		"{CLIP} plot -i {input} -o {output} -c r 2> {log}"
		
# ----------------------------------------------------------------------------------------	
# COUNT PLOTS
# ----------------------------------------------------------------------------------------	

rule count_plots:
	input:
		expand("{outdir}/plots/counts/{samples}_{sites}/{samples}_{sites}to{gtfn}_counts.html", samples=SAMPLES, gtfn=GTF_NAME, sites=SITES, outdir=OUT_DIR)
		
rule do_count_plot:
	input:
		"{OUT_DIR}/counts/{sample}_{site}to{gtfn}.count.txt.gz"
	output:
		"{OUT_DIR}/plots/counts/{sample}_{site}/{sample}_{site}to{gtfn}_counts.html"
	log:
		expand("{logs}/{{sample}}_{{site}}.cplot.log", logs = LOG_DIR)
	message:
		"creating count plot for {input}"
	shell:
		"mkdir -p {OUT_DIR}/plots/counts/{wildcards.sample}_{wildcards.site} && PYTHONPATH="" "
		"{CLIP} plot -i {input} -o {output} -c c 2> {log}"
		
# ----------------------------------------------------------------------------------------	
# JUNCTION PLOTS
# ----------------------------------------------------------------------------------------	

rule junction_plot:
	input:
		expand("{outdir}/plots/junction/{samples}_{sites}/{samples}_{sites}to{gtfn}_junction.html", samples=SAMPLES, gtfn=GTF_NAME, sites=SITES, outdir=OUT_DIR)
		
rule do_junction_plot:
	input:
		"{OUT_DIR}/junction/{sample}_{site}to{gtfn}.txt.gz"
	output:
		"{OUT_DIR}/plots/junction/{sample}_{site}/{sample}_{site}to{gtfn}_junction.html"
	log:
		expand("{logs}/{{sample}}_{{site}}.jplot.log", logs = LOG_DIR)
	message:
		"creating junction plot for {input}"
	shell:
		"PYTHONPATH="" "
		"{CLIP} plot -i {input} -o {output} -c j 2> {log}"
		
