---
title: "p62 iCLIP analysis"
author: "Thomas Schwarzl"
output: BiocStyle::html_document
---

## Setup

Loading required libraries for this analysis. 

```{r}
require(xlsx)
require(plyr)
require(dplyr)
require(DESeq2)
require(RColorBrewer)
require(ggplot2)
require(gplots)
require(knitr)
```

Sample names

```{r}
samples = c("DMSO-MBL-B",
            "DMSO-MBL-A",
            "DMSO-NOVUS-A",
            "DMSO-NOVUS-B",
            "Empty-A",
            "Empty-B",
            "IgG-M-A",
            "IgG-M-B",
            "IgG-R-A",
            "IgG-R-B")
```

We create a data frame containing sample information

```{r}
input.dir = "../htseq-clip/counts/"

y <- samples  
y[y == "Empty-A"] = "Empty-Empty-A"
y[y == "Empty-B"] = "Empty-Empty-B"
condition  <- unlist(lapply(strsplit(y, split="-"), function(x) x[[1]]))
antibody   <- unlist(lapply(strsplit(y, split="-"), function(x) x[[2]]))
replicates <- unlist(lapply(strsplit(y, split="-"), function(x) x[[3]]))

pheno <- data.frame(files = file.path("out", "counts", paste0(samples, "_S1togencode23lift37.count.txt.gz")),
                    names = samples,
                    condition = condition,
                    antibody = antibody,
                    replicates = replicates,
                    stringsAsFactors = F)

rownames(pheno) <- pheno$names
pheno[,"type"] <- pheno[,"condition"]
pheno[ pheno[,"type"] == "Empty" | pheno[,"type"] == "IgG","type"] = "Control"
kable(pheno)
```

## Analysis 

Uniquely mapped reads were downloaded from the iCount server (http://icount.fri.uni-lj.si/).Crosslink sites were extracted (one nucleotide upstream of the read start site) and converted to 'htseq-clip extract' format (https://bitbucket.org/htseq-clip/htseq-clip). 

The annotation was downloaded from Gencode (v23, liftover to GRCh37) and missannotated vaultRNAs gene types were fixed (script: vaultFix.py). tRNAscan data was downloaded from http://gtrnadb.ucsc.edu/ data base and a modified version of  https://github.com/jorvis/biocode/blob/master/gff/convert_tRNAScanSE_to_gff3.pl was used to convert it to GFF3 format. The Snakemake workflow and the scripts are available at https://git.embl.de/schwarzl/gencode-v23-tRNA-annotation.

The annotation was preprocessed with 'htseq-clip annotation' (https://bitbucket.org/htseq-clip/htseq-clip) and 
crosslink statistics were created with 'htseq-clip count'.

Here the crosslink sites are read 
```{r}
y <- lapply(pheno$files, function(file) {
    x <- read.delim(file, comment.char = '#', stringsAsFactors = F)
    as.data.frame(x[,c("Functional.type", "Total.cross.link.sites.in.region")] %>% group_by(Functional.type) %>% summarise_each(funs(sum)))
})

names(y) <- pheno$names
typelist   <- unique(sort(unlist(lapply(y, function(z) z[,1]))))

TYPES <-  data.frame(row.names = typelist)
TYPES[,pheno$names] <- 0

for(i in 1:length(x)) {
  TYPES[y[[i]][,1],names(y)[i]] <- y[[i]][,2]
}

TYPES <- TYPES[order(rowSums(TYPES)),]
TYPES

barplot(colSums(TYPES), las = 2)
```


Create Annotation for 
```{r}
z <- lapply(pheno$files, function(file) {
    x <- read.delim(file, comment.char = '#', stringsAsFactors = F)
    # remove intergenic
    x <- x[x[,"Functional.type"] != "intergenic",]
    x[,c(1:5,7:12)]
})
z <- do.call(rbind, z)
z <- z[order(z[,"Chromosome"], z[,"Region.start.pos"], z[,"Region.end.pos"]),]
ANNO <- unique(z)




zz <- lapply(pheno$files, function(file) {
    x <- read.delim(file, comment.char = '#', stringsAsFactors = F)
    # remove intergenic
    x <- x[x[,"Functional.type"] != "intergenic",]
    x[,c("Chromosome", "Region.start.pos", "Region.end.pos", "Gene.ID", "Strand", "Density")]
    #rownames(x) <- paste("Chromosome", "Region.start.pos", "Region.end.pos", "Gene.ID", "Strand"
})
names(zz) <- samples


DENSITIES <- ANNO
DENSITIES[  DENSITIES[,"Functional.type"] == "tRNAscan","Functional.type"] <- "tRNA"
for( i in samples ) {
    DENSITIES <- join(DENSITIES, zz[[ i ]], by = c("Chromosome", "Region.start.pos", "Region.end.pos", "Gene.ID", "Strand"))
    DENSITIES[is.na(DENSITIES[,"Density"]), "Density"] <- 0
    colnames(DENSITIES)[ colnames(DENSITIES) == "Density" ]  <- paste0(i, "-density")
}


#write.table(DENSITIES, file="densities-per-region.txt", sep="\t", quote=F, row.names=F) 
#write.xlsx(DENSITIES, file="densities-per-region.xlsx")
NULL
```


Counts to the Density regions

```{r}
zzz <- lapply(pheno$files, function(file) {
    x <- read.delim(file, comment.char = '#', stringsAsFactors = F)
    # remove intergenic
    x <- x[x[,"Functional.type"] != "intergenic",]
    x[,c("Chromosome", "Region.start.pos", "Region.end.pos", "Gene.ID", "Strand", "Total.cross.link.sites.in.region")]
})
names(zzz) <- samples

DCOUNTS <- DENSITIES
for( i in samples ) {
    DCOUNTS <- join(DCOUNTS, zzz[[ i ]], by = c("Chromosome", "Region.start.pos", "Region.end.pos", "Gene.ID", "Strand"))
    DCOUNTS[is.na(DCOUNTS[,"Total.cross.link.sites.in.region"]), "Total.cross.link.sites.in.region"] <- 0
    colnames(DCOUNTS)[ colnames(DCOUNTS) == "Total.cross.link.sites.in.region" ]  <- paste0(i, "-counts")
}




#DCOUNTS[  DCOUNTS[,"Functional.type"] == "tRNAscan" ] <- "tRNA"
write.xlsx(DCOUNTS, file="counts-per-region.xlsx")
write.table(DCOUNTS, file="per-region.txt", sep="\t", quote=F, row.names=F) 
NULL
```


```{r}

```


### Session Info

```{r}
sessionInfo()
```
